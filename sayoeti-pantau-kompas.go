/* Sayoeti Watch nasional.kompas.com
 *
 * Copyright 2015 Bayu Aldi Yansyah <bayualdiyansyah@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"github.com/pyk/sayoeti-api/database"
)

var (
	SAYOETI_PANTAU_DATABASE = os.Getenv("SAYOETI_PANTAU_DATABASE")
)

func main() {
	// Initialize database conn
	db, err := database.New(SAYOETI_PANTAU_DATABASE)
	if err != nil {
		log.Fatal(err)
	}

	// Make sure we connected
	if err := db.Conn.Ping(); err != nil {
		log.Fatal(err)
	}
	log.Println("sayoeti-pantau: [db] connected")

	// create publisher
	kompas := database.NewPublisherRecord("Kompas.com", "http://kompas.com", "kompas.com")
	exists, err := kompas.IsRecordExists(db)
	if err != nil {
		log.Fatal(err)
	}
	if !exists {
		if err := kompas.InsertRecord(db); err != nil {
			log.Fatal(err)
		}
	}

	if exists {
		kompas, err = database.GetPublisherRecordBySlug(db, kompas.Slug)
		if err != nil {
			log.Fatal(err)
		}
	}

	// prepare for id regex
	idRegex := regexp.MustCompile(`\d{6,10}`)

	// create sanitizer
	sanitizer := bluemonday.StrictPolicy()

	// for the first page
	for i := 0; i <= 2000; i += 25 {
		/* Build url for the first page */
		url := fmt.Sprintf("http://nasional.kompas.com/tag/kpk/%d", i)
		doc, err := goquery.NewDocument(url)
		if err != nil {
			log.Fatal(err)
		}

		// for each link in the page, print the title & link
		doc.Find(".list-latest > a").Each(func(i int, s *goquery.Selection) {

			// get article URL
			articleURL, exists := s.Attr("href")
			if !exists {
				fmt.Println("sayoeti-pantau: error: article url not exists")
				return
			}

			// get article id from URL
			articleID := idRegex.FindString(articleURL)
			if articleID == "" {
				fmt.Printf("sayoeti-pantau: error: article id not found in %s\n", articleURL)
				return
			}

			fmt.Printf("sayoeti-pantau: GET %s:%s\n", articleID, articleURL)

			// fetch article content
			article, err := goquery.NewDocument(articleURL)
			if err != nil {
				fmt.Printf("sayoeti-pantau: error: couldn't create new document from %s\n", articleURL)
				return
			}

			// Get title
			title := article.Find(".kcm-read-top > h2:nth-child(2)").First().Text()

			// Get Image URL
			imageURL, exists := article.Find(".photo > img").First().Attr("src")
			if !exists {
				fmt.Println("sayoeti-pantau: error: article image url not exists")
			}

			// Get the content
			content := strings.TrimSpace(sanitizer.Sanitize(article.Find(".kcm-read-text").First().Text()))

			// add news to the database
			news := database.NewNewsRecord(title, content, articleURL, imageURL, kompas)
			newsExist, err := news.IsRecordExists(db)
			if err != nil {
				log.Fatal(err)
			}
			if newsExist {
				log.Fatal("sayoeti-pantau: news already uptodate. exit.")
			}

			if !newsExist {
				if err := news.InsertRecord(db); err != nil {
					log.Fatal(err)
				}
			}

		})
	}
}
