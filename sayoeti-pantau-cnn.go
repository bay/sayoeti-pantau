/* Sayoeti Watch CNN Indonesia
 *
 * Copyright 2015 Bayu Aldi Yansyah <bayualdiyansyah@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"github.com/pyk/sayoeti-api/database"
)

var (
	SAYOETI_PANTAU_DATABASE = os.Getenv("SAYOETI_PANTAU_DATABASE")
)

func main() {
	// Initialize database conn
	db, err := database.New(SAYOETI_PANTAU_DATABASE)
	if err != nil {
		log.Fatal(err)
	}

	// Make sure we connected
	if err := db.Conn.Ping(); err != nil {
		log.Fatal(err)
	}
	log.Println("sayoeti-pantau: [db] connected")

	// create publisher
	cnnID := database.NewPublisherRecord("CNN Indonesia", "http://www.cnnindonesia.com/", "cnnindonesia.com")
	exists, err := cnnID.IsRecordExists(db)
	if err != nil {
		log.Fatal(err)
	}
	if !exists {
		if err := cnnID.InsertRecord(db); err != nil {
			log.Fatal(err)
		}
	}

	if exists {
		cnnID, err = database.GetPublisherRecordBySlug(db, cnnID.Slug)
		if err != nil {
			log.Fatal(err)
		}
	}

	// prepare for id regex
	idRegex := regexp.MustCompile(`\d{6,10}`)

	// create sanitizer
	sanitizer := bluemonday.StrictPolicy()

	// for the first page
	for i := 1; i <= 100; i++ {
		/* Build url for the first page */
		url := fmt.Sprintf("http://www.cnnindonesia.com/search/article?query=kpk&page=%d", i)
		doc, err := goquery.NewDocument(url)
		if err != nil {
			log.Fatal(err)
		}

		// for each link in the page, print the title & link
		doc.Find("body > div.container > div.wrapper > div.tengah2 > ul.content > li > div.title > a").Each(func(i int, s *goquery.Selection) {

			// get article URL
			articleURL, exists := s.Attr("href")
			if !exists {
				fmt.Println("sayoeti-pantau: error: article url not exists")
				return
			}

			// get article id from URL
			articleID := idRegex.FindString(articleURL)
			if articleID == "" {
				fmt.Printf("sayoeti-pantau: error: article id not found in %s\n", articleURL)
				return
			}

			fmt.Printf("sayoeti-pantau: GET %s:%s\n", articleID, articleURL)

			// fetch article content
			article, err := goquery.NewDocument(articleURL)
			if err != nil {
				fmt.Printf("sayoeti-pantau: error: couldn't create new document from %s\n", articleURL)
				return
			}

			// Get title
			title := article.Find("body > div.container > div > div.kiri.gariskanan > div.content_detail > h1").First().Text()

			// Get Image URL
			imageURL, exists := article.Find("body > div.container > div > div.kiri.gariskanan > div.content_detail > div.checkOrientation.pic_artikel > img").First().Attr("src")
			if !exists {
				fmt.Println("sayoeti-pantau: error: article image url not exists")
			}

			// Get the content
			content := strings.TrimSpace(sanitizer.Sanitize(article.Find("#detail").First().Text()))

			// add news to the database
			news := database.NewNewsRecord(title, content, articleURL, imageURL, cnnID)
			newsExist, err := news.IsRecordExists(db)
			if err != nil {
				log.Fatal(err)
			}
			if newsExist {
				log.Fatal("sayoeti-pantau: news already uptodate. exit.")
			}

			if !newsExist {
				if err := news.InsertRecord(db); err != nil {
					log.Fatal(err)
				}
			}

		})
	}
}
