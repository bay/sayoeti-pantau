# usage
Edit the `shell.env` & `systemd.env` then run this consquence command

    go run sayoeti-pantau-kompas.go
    go run sayoeti-pantau-antaranews.go
    go run sayoeti-pantau-tribuntimur.go
    go run sayoeti-pantau-serambiindonesia.go
    go run sayoeti-pantau-bangkapos.go
    go run sayoeti-pantau-viva.go
    go run sayoeti-pantau-cnn.go
    go run sayoeti-pantau-wartakota.go

    go build sayoeti-pantau-kompas.go
    go build sayoeti-pantau-antaranews.go
    go build sayoeti-pantau-tribuntimur.go
    go build sayoeti-pantau-serambiindonesia.go
    go build sayoeti-pantau-bangkapos.go
    go build sayoeti-pantau-viva.go
    go build sayoeti-pantau-cnn.go
    go build sayoeti-pantau-wartakota.go

    # for sudo
    sudo cp sayoeti-pantau-kompas.* /etc/systemd/system
    sudo systemctl enable sayoeti-pantau-kompas.service
    sudo systemctl enable sayoeti-pantau-kompas.timer
    sudo systemctl start sayoeti-pantau-kompas.timer
    
    # for sudo
    sudo cp sayoeti-pantau-antaranews.* /etc/systemd/system
    sudo systemctl enable sayoeti-pantau-antaranews.service
    sudo systemctl enable sayoeti-pantau-antaranews.timer
    sudo systemctl start sayoeti-pantau-antaranews.timer

    # for sudo
    sudo cp sayoeti-pantau-tribuntimur.* /etc/systemd/system
    sudo systemctl enable sayoeti-pantau-tribuntimur.service
    sudo systemctl enable sayoeti-pantau-tribuntimur.timer
    sudo systemctl start sayoeti-pantau-tribuntimur.timer

    # for sudo
    sudo cp sayoeti-pantau-serambiindonesia.* /etc/systemd/system
    sudo systemctl enable sayoeti-pantau-serambiindonesia.service
    sudo systemctl enable sayoeti-pantau-serambiindonesia.timer
    sudo systemctl start sayoeti-pantau-serambiindonesia.timer

    # for sudo
    sudo cp sayoeti-pantau-bangkapos.* /etc/systemd/system
    sudo systemctl enable sayoeti-pantau-bangkapos.service
    sudo systemctl enable sayoeti-pantau-bangkapos.timer
    sudo systemctl start sayoeti-pantau-bangkapos.timer

    # for sudo
    sudo cp sayoeti-pantau-viva.* /etc/systemd/system
    sudo systemctl enable sayoeti-pantau-viva.service
    sudo systemctl enable sayoeti-pantau-viva.timer
    sudo systemctl start sayoeti-pantau-viva.timer

    # for sudo
    sudo cp sayoeti-pantau-cnn.* /etc/systemd/system
    sudo systemctl enable sayoeti-pantau-cnn.service
    sudo systemctl enable sayoeti-pantau-cnn.timer
    sudo systemctl start sayoeti-pantau-cnn.timer

    # for sudo
    sudo cp sayoeti-pantau-wartakota.* /etc/systemd/system
    sudo systemctl enable sayoeti-pantau-wartakota.service
    sudo systemctl enable sayoeti-pantau-wartakota.timer
    sudo systemctl start sayoeti-pantau-wartakota.timer
    
    # non sudo
    cp sayoeti-pantau-kompas.* /etc/systemd/system
    systemctl enable sayoeti-pantau-kompas.service
    systemctl enable sayoeti-pantau-kompas.timer
    systemctl start sayoeti-pantau-kompas.timer
    
    # non sudo
    cp sayoeti-pantau-antaranews.* /etc/systemd/system
    systemctl enable sayoeti-pantau-antaranews.service
    systemctl enable sayoeti-pantau-antaranews.timer
    systemctl start sayoeti-pantau-antaranews.timer
    
    # non sudo
    cp sayoeti-pantau-tribuntimur.* /etc/systemd/system
    systemctl enable sayoeti-pantau-tribuntimur.service
    systemctl enable sayoeti-pantau-tribuntimur.timer
    systemctl start sayoeti-pantau-tribuntimur.timer
    
    # non sudo
    cp sayoeti-pantau-serambiindonesia.* /etc/systemd/system
    systemctl enable sayoeti-pantau-serambiindonesia.service
    systemctl enable sayoeti-pantau-serambiindonesia.timer
    systemctl start sayoeti-pantau-serambiindonesia.timer
    
    # non sudo
    cp sayoeti-pantau-bangkapos.* /etc/systemd/system
    systemctl enable sayoeti-pantau-bangkapos.service
    systemctl enable sayoeti-pantau-bangkapos.timer
    systemctl start sayoeti-pantau-bangkapos.timer
    
    # non sudo
    cp sayoeti-pantau-viva.* /etc/systemd/system
    systemctl enable sayoeti-pantau-viva.service
    systemctl enable sayoeti-pantau-viva.timer
    systemctl start sayoeti-pantau-viva.timer
    
    # non sudo
    cp sayoeti-pantau-cnn.* /etc/systemd/system
    systemctl enable sayoeti-pantau-cnn.service
    systemctl enable sayoeti-pantau-cnn.timer
    systemctl start sayoeti-pantau-cnn.timer
    
    # non sudo
    cp sayoeti-pantau-wartakota.* /etc/systemd/system
    systemctl enable sayoeti-pantau-wartakota.service
    systemctl enable sayoeti-pantau-wartakota.timer
    systemctl start sayoeti-pantau-wartakota.timer

