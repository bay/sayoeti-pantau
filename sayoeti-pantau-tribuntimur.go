/* Sayoeti Watch makassar.tribunnews.com
 *
 * Copyright 2015 Bayu Aldi Yansyah <bayualdiyansyah@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"github.com/pyk/sayoeti-api/database"
)

var (
	SAYOETI_PANTAU_DATABASE = os.Getenv("SAYOETI_PANTAU_DATABASE")
)

func main() {
	// Initialize database conn
	db, err := database.New(SAYOETI_PANTAU_DATABASE)
	if err != nil {
		log.Fatal(err)
	}

	// Make sure we connected
	if err := db.Conn.Ping(); err != nil {
		log.Fatal(err)
	}
	log.Println("sayoeti-pantau: [db] connected")

	// create publisher
	tribunT := database.NewPublisherRecord("Tribun Timur", "http://makassar.tribunnews.com/", "makassar.tribunnews.com")
	exists, err := tribunT.IsRecordExists(db)
	if err != nil {
		log.Fatal(err)
	}
	if !exists {
		if err := tribunT.InsertRecord(db); err != nil {
			log.Fatal(err)
		}
	}

	if exists {
		tribunT, err = database.GetPublisherRecordBySlug(db, tribunT.Slug)
		if err != nil {
			log.Fatal(err)
		}
	}

	// create sanitizer
	sanitizer := bluemonday.StrictPolicy()

	// for the first page
	for i := 1; i <= 20; i++ {
		/* Build url for the first page */
		url := fmt.Sprintf("http://makassar.tribunnews.com/tag/kpk?page=%d", i)
		doc, err := goquery.NewDocument(url)
		if err != nil {
			log.Fatal(err)
		}

		// for each link in the page, print the title & link
		doc.Find(".mr140 > a").Each(func(i int, s *goquery.Selection) {

			// get article URL
			articleURL, exists := s.Attr("href")
			if !exists {
				fmt.Println("sayoeti-pantau: error: article url not exists")
				return
			}

			fmt.Printf("sayoeti-pantau: GET %s\n", articleURL)

			// fetch article content
			article, err := goquery.NewDocument(articleURL)
			if err != nil {
				fmt.Printf("sayoeti-pantau: error: couldn't create new document from %s\n", articleURL)
				return
			}

			// Get title
			title := article.Find("#arttitle").First().Text()

			// Get Image URL
			imageURL, exists := article.Find("#artimg > div > div.ovh.imgfull_div > img").First().Attr("src")
			if !exists {
				fmt.Println("sayoeti-pantau: error: article image url not exists")
			}

			// Get the content
			content := strings.TrimSpace(sanitizer.Sanitize(article.Find("#article_con > div.side-article.txt-article").First().Text()))

			// add news to the database
			news := database.NewNewsRecord(title, content, articleURL, imageURL, tribunT)
			newsExist, err := news.IsRecordExists(db)
			if err != nil {
				log.Fatal(err)
			}
			if newsExist {
				log.Fatal("sayoeti-pantau: news already uptodate. exit.")
			}

			if !newsExist {
				if err := news.InsertRecord(db); err != nil {
					log.Fatal(err)
				}
			}

		})
	}
}
